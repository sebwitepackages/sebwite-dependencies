<?php
/**
 * Part of the CLI PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Tests\Dependencies\Fixture;

use Sebwite\Contracts\Dependencies\Dependable;

class SimpleDependable implements Dependable
{
    protected $handle;

    protected $deps;

    public function __construct($handle, $deps = [ ])
    {
        $this->handle = $handle;
        $this->deps   = $deps;
    }

    public function getDependencies()
    {
        return $this->deps;
    }

    public function getHandle()
    {
        return $this->handle;
    }
}
